using CheckSum.Model;

namespace CheckSum
{
    public interface IHashBuilder
    {
        HashResult BuildHash();
    }
}