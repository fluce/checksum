using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckSum.Model
{
    public class HashResult
    {
        public List<HashValue> DetailedHashValues;
        public string GlobalHash;

        public string DetailedHashValuesAsString
        {
            get
            {
                return DetailedHashValues.Select(x => string.Format("{0};{1}", x.FileName, x.Hash))
                    .Aggregate(new StringBuilder(), (a, i) => a.Append(i + "\r\n")).ToString();
            }
        }
    }
}